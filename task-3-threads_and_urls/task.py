#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create a download manager, that will download html page by http url and
save it to the file in the background. Download can't block and doesn't need
to support a way to get its result. (hint: use can use threads and httplib)

Example usage:

dm = DownloadManager()
dm.download('http://www.pythonforbeginners.com/', 'pb.html')
dm.download('https://howtodoinjava.com', 'java.html')
dm.download('https://stackoverflow.com', 'so.html')
"""
